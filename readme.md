# FORMULARIO DE SELECCION #

Para comprender el funcionamiento de un formulario con opciones, se procede a generar un chatbox en donde no se podrá enviar un mensaje si un RUT emplea el mismo alias, pero un mismo alias podrá ser empleado por uno o varios RUT. Unos checkbox que permitirán enviar mensajes en distintos colores y ver la estadística de mensajes enviados. Se emplean conceptos de validaciones por Js encapsulando métodos y también en las capas php y base de datos. Además se podrá exportar el total de conversaciones en un archivo *xml.

### Codificado en ISO 8859-9 salvo esta hoja UTF-8
### Tecnologias utilizadas ###

* PHP (incluyendo HTML5)
* Js (no JQuery) -  AJAX
* Postgres SQL
* Generación de archivos XML

###  ( ͡° ͜ʖ ͡°) ###

* ( ͡° ͜ʖ ͡°)
* ( ͡° ͜ʖ ͡°)
* ( ͡°( ͡° ͜ʖ( ͡° ͜ʖ ͡°)ʖ ͡°) ͡°)
* ( ͡° ͜ʖ ͡°)