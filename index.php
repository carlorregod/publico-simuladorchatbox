<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO 8859-1"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Chatbox</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
</head>
<body>
    <table>
        <tr>
            <td>
                <!--Columna 1 contiene datos para enviar mensajes chat-->
                <table><p class="titulo_ppal">Chat</p>
                    <tbody>
                        <tr>
                            <td><label for="nombre">Nombre y apellido</label></td>
                            <td><label for="alias">Alias</label></td>
                        </tr>
                        <tr>
                            <td><input type="text" class="controlFormularioTabla1" name="nombre" id="nombre" onkeypress="return soloLetrasNombre(event)"/></td>
                            <td><input type="text" class="controlFormularioTabla1" name="alias" id="alias" onkeypress="return soloNumerosLetras(event)"/></td> 
                        </tr>
                        <tr>
                            <td><label for="rut">Rut</label></td>
                            <td><label for="email">Email</label></td>
                        </tr>
                        <tr>
                            <td><input type="text" class="controlFormularioTabla1" name="rut" id="rut" onkeypress="return soloNumerosRut(event)"/></td>
                            <td><input type="email" class="controlFormularioTabla1" name="email" id="email" onkeypress="return ingresoCorreoElectronico(event)"/></td>
                        </tr>
                        <tr>
                            <td><label for="pais">Pa�s</label></td>
                            <td><label for="ciudad">Ciudad</label></td>
                        </tr>
                        <tr>
                            <td><select class="controlFormularioTabla1" name="pais" id="pais"></select></td>                     
                            <td><select class="controlFormularioTabla1" name="ciudad" id="ciudad"></select></td>
                        </tr>
                        <tr>
                            <td colspan="2">Color:&nbsp
                            <label><input type="checkbox" name="colorrojo" id="colorrojo" onchange="validarojo()">Rojo</label>
                            <label><input type="checkbox" name="colornegro" id="colornegro" onchange="validanegro()">Negro</label>
                            <label><input type="checkbox" name="colorverde" id="colorverde" onchange="validaverde()">Verde</label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"><label for="mensaje">Mensaje</label></td>
                        </tr>
                        <tr>
                            <td colspan="2"><textarea cols="25" rows="3" class="textbox_mensaje" name="mensaje" id="mensaje" /></textarea></td>
                        </tr>
                        <tr>
                            <td colspan="2"><input type="button" class="botonEnviar" value="Enviar" onclick="return enviar()"></td>
                        </tr>
                    </tbody>
                </table><br>
            </td>
            <td id="tablaUsuariosConectadosOrientacion">
                <!--Columna 2 contiene la conversaci�n del chat para enviar el XML-->
                <table id="tablaUsuariosConectados">
                    <thead>
                        <tr>
                            <th class="formatoEncaebzadoTabla"><label id="titulosDeTabla">Usuarios</label></th>
                            <th class="formatoEncaebzadoTabla"><label id="titulosDeTabla">Conectados</label></th>
                        </tr>
                    </thead>
                    <tbody class="usuariosConectados" id="usuariosConectados">
                        <!-- Autocompletado de los que est�n ON...-->
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    <!--Conversaciones del chat en tabla inferior-->
    <table id="tablaConversacionesChat">
        <thead>
            <tr><th colspan="9" class="formatoEncaebzadoTabla"><label id="titulosDeTabla2">Conversaci�n</label></th></tr>
        </thead>
        <tbody class="conversacionesGuardadas" id="conversacionesGuardadas">
        <!-- Autocompletado de las conversaciones...-->
        </tbody>
    </table>
    <input type="button" class="botonGeneraXML" value="XMLexport" onclick="return descargarXML()">
</body>
<footer>
    <script src="js/utils.js"></script>
    <script src="js/Controlador.js"></script>
</footer>
</html>