--Se trabajar� en un esquema distinto--
CREATE SCHEMA chat AUTHORIZATION candidato;
--Creaci�n de tabla pa�s y ocurrencias--
DROP TABLE IF EXISTS chat.Pais
CREATE TABLE chat.Pais(
    idpais serial not null primary key,
    nombrepais varchar(40)
);
INSERT INTO chat.Pais(nombrepais)
VALUES ('Chile'), ('Argentina'), ('Per�'),
 ('Mexico'), ('Colombia');

DROP TABLE IF EXISTS chat.Ciudad CASCADE;
CREATE TABLE chat.Ciudad(
    idciudad serial not null primary key,
    idpais integer,
    nombreciudad varchar(40)
);

ALTER TABLE chat.Ciudad
ADD CONSTRAINT FK_pais
foreign key(idpais) 
references chat.Pais(idpais);

INSERT INTO chat.Ciudad(idpais, nombreciudad)
VALUES (1,'Santiago'), (1,'Vi�a del mar'), (1,'Concepci�n'), (1,'Cartagua'), 
       (2,'Buenos Aires'), (2,'Mendoza'), (2,'C�rdoba'), 
       (3,'Cuzco'), (3,'Tacna'), (3,'Lima'), 
       (4,'Mexico D.F.'), (4,'Acapulco'), 
       (5,'Medell�n');

--VISTA CONSOLIDADA DE CIUDADES Y PAISES--No se pide pero se usar�
CREATE VIEW chat.vw_PaisCiudad AS
SELECT chat.Ciudad.idciudad, chat.Pais.idpais, chat.Ciudad.nombreciudad, chat.Pais.nombrepais FROM chat.Ciudad
JOIN chat.Pais
ON (chat.Pais.idpais = chat.Ciudad.idpais);

--Creaci�n de las tablas asociadas al chat

DROP TABLE IF EXISTS chat.Color;
CREATE TABLE chat.Color(
    idcolor serial NOT NULL PRIMARY KEY,
    nombrecolor VARCHAR NOT NULL
);

INSERT INTO chat.Color(nombrecolor) VALUES ('Rojo'), ('Negro'), ('Verde');

DROP TABLE IF EXISTS chat.Persona;
CREATE TABLE chat.Persona(
    idpersona serial NOT NULL PRIMARY KEY,
    nombreapellido VARCHAR(100) NOT NULL,
    rut VARCHAR(10) NOT NULL,
    email VARCHAR(60) NOT NULL
);

DROP TABLE IF EXISTS chat.Chat;
CREATE TABLE chat.Chat(
    idchat serial NOT NULL PRIMARY KEY,
    idpersona INTEGER, --FK
    alias VARCHAR(30) NOT NULL,
    idciudad INTEGER NOT NULL, --FK
    mensaje varchar(200),
    idcolor INTEGER, --(fk)
    fechahora TIMESTAMP DEFAULT NOW(),
    eliminado BOOLEAN DEFAULT FALSE
);

ALTER TABLE chat.Chat
ADD CONSTRAINT FK_persona
FOREIGN KEY(idpersona) 
REFERENCES chat.Persona(idpersona);
ALTER TABLE chat.Chat
ADD CONSTRAINT FK_ciudad
FOREIGN KEY(idciudad) 
REFERENCES chat.Ciudad(idciudad);
ALTER TABLE chat.Chat
ADD CONSTRAINT FK_color
FOREIGN KEY(idcolor) 
REFERENCES chat.Color(idcolor);

--VISTAS OBLIGATORIAS--HAY QUE ACTUALIZAR LAS VISTAS!!!
DROP VIEW IF EXISTS chat.vw_listachats CASCADE;
CREATE VIEW chat.vw_listachats AS
SELECT idchat, alias, nombrepais, fechahora, mensaje, nombrecolor FROM chat.Chat
JOIN chat.Ciudad
ON (chat.Ciudad.idciudad = chat.Chat.idciudad)
JOIN chat.Pais
ON (chat.Pais.idpais = chat.Ciudad.idpais)
JOIN chat.Color
ON (chat.Chat.idcolor = chat.Color.idcolor)
WHERE chat.Chat.eliminado = FALSE
ORDER BY idchat DESC;
----ESTA HAY QUE UPDATE
DROP VIEW chat.vw_totalchats;
CREATE VIEW chat.vw_totalchats AS
SELECT Pais.nombrepais, COALESCE(C,0) AS conectados  FROM chat.Pais
LEFT JOIN (SELECT nombrepais, count(*) as C FROM chat.vw_listachats GROUP BY nombrepais) AS tablaresumen
ON (Pais.nombrepais =tablaresumen.nombrepais) 
ORDER BY conectados DESC;
----
DROP VIEW IF EXISTS chat.vw_xml;
--HAY QUE ACTUALIZAR ESTA VISTA PARA QUE TODO FUNCIONE--
CREATE VIEW chat.vw_xml AS
SELECT idchat, nombreapellido, alias, rut, email, Pais.idpais, nombrepais, Chat.idciudad, nombreciudad, nombrecolor AS color, mensaje, fechahora AS fecha FROM chat.Chat
JOIN chat.Persona ON (chat.Chat.idpersona=chat.Persona.idpersona)
JOIN chat.Ciudad ON (chat.Chat.idciudad=chat.Ciudad.idciudad)
JOIN chat.Pais ON (chat.ciudad.idpais = chat.Pais.idpais)
JOIN chat.Color ON (chat.Color.idcolor = chat.Chat.idcolor)
ORDER BY idchat ASC;
-- EjemplO: SELECT * FROM chat.vw_xml;

--  FUNCIONES OBLIGATORIAS
--Eliminar registro
DROP FUNCTION IF EXISTS chat.fn_chat_d(INTEGER, VARCHAR);
CREATE OR REPLACE FUNCTION chat.fn_chat_d (INTEGER, VARCHAR) 
RETURNS VOID AS
$body$
DECLARE
_idchat ALIAS FOR $1; --ID para eliminado
_borrado ALIAS FOR $2; --borrar es el valor que necesita para eliminar
BEGIN
  IF (_borrado = 'borrar') THEN
	  DELETE FROM chat.Chat WHERE idchat= _idchat;
  ELSE
      UPDATE chat.Chat SET eliminado=TRUE WHERE idchat= _idchat;
  END IF;
END;
$body$ 
LANGUAGE 'plpgsql' volatile;

--Ejemplo: SELECT chat.fn_chat_d(5,'borrar');

--Consultar alias 
-- DROP FUNCTION IF EXISTS chat.fn_chat_consultaalias__(VARCHAR, VARCHAR);
-- CREATE OR REPLACE FUNCTION chat.fn_chat_consultaalias__(VARCHAR, VARCHAR) 
-- RETURNS BOOLEAN AS
-- $body$
-- DECLARE
-- _rut ALIAS FOR $1;
-- _alias ALIAS FOR $2;
-- BEGIN
-- PERFORM alias FROM chat.Chat JOIN chat.Persona 
-- ON (chat.Chat.idpersona = chat.Persona.idpersona)
-- WHERE rut= _rut AND eliminado=false AND alias= _alias;
-- RETURN FOUND;
-- END;
-- $body$
-- LANGUAGE 'plpgsql' volatile;

--SELECT chat.fn_chat_consultaalias('15469832-9', 'goku');

DROP FUNCTION IF EXISTS chat.fn_chat_consultaalias(VARCHAR, VARCHAR);
CREATE OR REPLACE FUNCTION chat.fn_chat_consultaalias(VARCHAR, VARCHAR) 
RETURNS BOOLEAN AS
$body$
DECLARE
_rut ALIAS FOR $1;
_alias ALIAS FOR $2;
__query VARCHAR;
BEGIN
__query := (SELECT alias FROM chat.Chat JOIN chat.Persona ON (chat.Chat.idpersona = chat.Persona.idpersona) WHERE rut= _rut AND eliminado=false AND alias= _alias LIMIT 1);
IF (__query IS NULL) THEN
	return TRUE; --Alias disponible
ELSE
	return FALSE; --Alias ya empleado para ese RUT
END IF;
END;
$body$
LANGUAGE 'plpgsql' volatile;

-- EJEMPLO: SELECT chat.fn_chat_consultaalias('15469832-9', 'sdada');

--OTRAS VALIDACIONES NO OBLIGATORIAS PERO QUE HAY QUE HACER--

--*****AVERIGUANDO SI EL RUT YA EXISTE
DROP FUNCTION IF EXISTS chat.fn_chat_consultapersona(VARCHAR);
CREATE OR REPLACE FUNCTION chat.fn_chat_consultapersona(VARCHAR) 
RETURNS BOOLEAN AS
$body$
DECLARE
_rut ALIAS FOR $1;
__query VARCHAR;
BEGIN
__query := (SELECT rut FROM chat.Persona WHERE rut= _rut LIMIT 1);
IF (__query IS NULL) THEN
	return TRUE; --RUT disponible
ELSE
	return FALSE; --RUT ya existe
END IF;
END;
$body$
LANGUAGE 'plpgsql' volatile;
--EJEMPLO: SELECT chat.fn_chat_consultapersona('15469832-9');

--*****VALIDANDO EL RUT
DROP FUNCTION IF EXISTS chat.sp_rut_cl(VARCHAR);
CREATE OR REPLACE FUNCTION chat.sp_rut_cl(
          rut VARCHAR
          ) RETURNS CHARACTER(1)
   AS
$BODY$
DECLARE
  rec record;
  suma INTEGER := 0;
  serie INTEGER := 2;
  resto INTEGER;
  dv CHARACTER(1);
BEGIN
  --raise notice 'rut: %',rut;
  if (rut is null) then
    return null;
  end if;
  rut := btrim(rut);
  rut := replace(rut, '.', '');
  if (rut is null) then
    return null;
  end if;
  rut := btrim(rut);
  for rec in select * from (
              select substring(rut from i for 1)::char as bit
              from generate_series(length(rut),1,-1) as i
              --where bit = '1'
            ) q1 LOOP
            --raise notice '1';
            --raise notice 'rec.bit: %',rec.bit;
            --raise notice '2';
            if rec.bit is not null and rec.bit ~ '[0-9]+' then
		suma := suma + rec.bit::INTEGER * serie;
            end if;
            --raise notice '3';
            --raise notice 'serie: %',serie;
            if serie = 7 then 
              serie := 1;
            end if;
            serie := serie + 1;
  end loop;
  --raise notice 'suma: %',suma;
  resto := 11 - suma % 11;
  --raise notice 'resto: %',resto;
  dv := case resto when 11 then '0' when 10 then 'K' else resto::CHARACTER end;
  return dv;
end;
$BODY$ LANGUAGE 'plpgsql' volatile;
--Funci�n propia para validar el rut
DROP FUNCTION IF EXISTS chat.fn_validaRut_r(VARCHAR);
CREATE OR REPLACE FUNCTION chat.fn_validaRut_r(VARCHAR) RETURNS BOOLEAN AS
$body$
DECLARE
_rut ALIAS FOR $1;
__formatoRUT VARCHAR;
__dv_calculado CHARACTER;
__dv CHARACTER;
__esRut BOOLEAN;
BEGIN
IF (textregexeq(_rut,'^[1-9][0-9]{6,7}[-]([0-9]|[kK])$')=false) THEN
	__esRut=false;
ELSE
	__formatoRUT:=substr(_rut,1,length(_rut)-2);
    --Ojo que se llama ac� a la funci�n de m�s ariba
	__dv := chat.sp_rut_cl(__formatoRUT);               
	__dv_calculado := substr(_rut,length(_rut),1);
	IF (__dv = __dv_calculado) THEN
		__esRut= true;
	ELSE
		__esRut= false;
	END IF;
END IF;
RETURN __esRut;
END;
$body$ LANGUAGE 'plpgsql' volatile;
--Ejemplo: select chat.fn_validaRut('15469832-8') da false; pero select fn_validaRut('15469832-9')da true

--**VALIDANDO EL CORREO
DROP FUNCTION IF EXISTS chat.fn_validacorreo_r(VARCHAR) ;
CREATE OR REPLACE FUNCTION chat.fn_validaCorreo_r (VARCHAR) 
RETURNS BOOLEAN AS
	$body$
		DECLARE
		--Declaraci�n de la variable de entrada
		_email ALIAS FOR $1;
		--Declaraci�n de las variables locales de la funci�n
		__esCorreo BOOLEAN;
		BEGIN
			--IF (textregexeq(_email,'^([a-zA-Z])([\w.-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$')=true) THEN
			IF (textregexeq(_email, E'^([a-zA-Z])([a-zA-Z0-9_.-])+\@(([a-zA-Z0-9\-])+([\.]){1})([a-zA-Z0-9]{2,4})+$') = TRUE) THEN
        __esCorreo :=true;
			ELSE
				__esCorreo :=false;
			END IF;
		RETURN __esCorreo;
		END;
	$body$
Language 'plpgsql';
--Ejemplo: select chat.fn_validaCorreo_r('alguien1@gmail.com');

--*****VALIDAR NOMBRE
DROP FUNCTION IF EXISTS chat.fn_validaNombre_r(VARCHAR);
CREATE OR REPLACE FUNCTION chat.fn_validaNombre_r(VARCHAR) RETURNS BOOLEAN AS
$body$
DECLARE
_nombre ALIAS FOR $1;
__esNombre BOOLEAN;
BEGIN
IF (textregexeq(_nombre, E'^([a-zA-Z������������])+([ ]){1}([a-zA-Z������������\ ])+')=TRUE) THEN
	__esNombre=TRUE;
ELSE
	__esNombre=FALSE;
END IF;
RETURN __esNombre;
END;
$body$ LANGUAGE 'plpgsql' volatile;
--Ejemplo: SELECT chat.fn_validaNombre_r('Asyo soy');

--**VALIDANDO AL ALIAS
DROP FUNCTION IF EXISTS chat.fn_validaAlias_r(VARCHAR);
CREATE OR REPLACE FUNCTION chat.fn_validaAlias_r(VARCHAR) 
RETURNS INTEGER AS
$body$
DECLARE
_nickname ALIAS FOR $1;
BEGIN
IF (textregexeq(_nickname, E'([a-zA-Z������������]+[0-9]+)+|([0-9]+[a-zA-Z������������]+)')=FALSE) THEN
    RETURN 1;
ELSIF (length(_nickname)<6) THEN
    RETURN 2;
ELSE
    RETURN 3;
END IF;
END;
$body$ LANGUAGE 'plpgsql' volatile;
--Ejemplo: SELECT chat.fn_validaAlias_r('gw3e5'); (DA 2)

--**VALIDANDO QUE LOS CHECKBOX SEAN SELECCIONADOS * ejecutar
DROP FUNCTION IF EXISTS chat.fn_checkboxColor_r(INTEGER);
CREATE OR REPLACE FUNCTION chat.fn_checkboxColor_r(INTEGER) 
RETURNS BOOLEAN AS
$body$
DECLARE
_color ALIAS FOR $1;
BEGIN
IF (_color = 1 OR _color = 2 OR _color = 3) THEN
	RETURN TRUE;
ELSE
    RETURN FALSE;
END IF;
END;
$body$ LANGUAGE 'plpgsql' volatile;
--Ejemplo: SELECT chat.fn_checkboxColor_r(2); (SER� TRUE)

--Funci�n de inserci�n y actualizaci�n (OBLIGATORIA)
DROP FUNCTION IF EXISTS chat.fn_chat_iu(VARCHAR, VARCHAR, VARCHAR, VARCHAR, INTEGER, INTEGER, INTEGER, VARCHAR);
CREATE OR REPLACE FUNCTION chat.fn_chat_iu(VARCHAR, VARCHAR, VARCHAR, VARCHAR, INTEGER, INTEGER, INTEGER, VARCHAR) 
RETURNS INTEGER AS
$body$
DECLARE
--Variables internas de entrada
_nombre ALIAS FOR $1;
_alias ALIAS FOR $2;
_rut ALIAS FOR $3;
_correo ALIAS FOR $4;
_pais ALIAS FOR $5;
_ciudad ALIAS FOR $6;
_color ALIAS FOR $7;
_mensaje ALIAS FOR $8;
--Variables locales
__idpersona INTEGER;
__respuesta INTEGER;
BEGIN
--Verificaci�n de las validaciones
IF(_nombre IS NULL OR _alias IS NULL OR _rut IS NULL OR _correo IS NULL OR _pais =0 OR _ciudad=0 OR _color IS NULL) THEN
	__respuesta:= -1; --Campos vac�os
ELSIF (SELECT NOT chat.fn_validaNombre_r(_nombre)) THEN
    __respuesta:= 1;  --Nombre vac�o
ELSIF (SELECT chat.fn_validaAlias_r(_alias) = 1 ) THEN
     __respuesta:=2;   --Alias no tiene letra+n�mero
ELSIF (SELECT chat.fn_validaAlias_r(_alias) = 2 ) THEN
     __respuesta:=3;   --Alias no posee largo establecido
ELSIF (SELECT NOT chat.fn_validaRut_r(_rut)) THEN
     __respuesta:=4;   --RUT no v�lido
ELSIF (SELECT NOT chat.fn_validacorreo_r(_correo)) THEN
     __respuesta:=5;   --Correo no v�lido
ELSIF (_mensaje IS NULL OR _mensaje = '') THEN
     __respuesta:=6;    --Mensaje vac�o se controlar� dedicadamente
ELSIF (SELECT NOT chat.fn_checkboxColor_r(_color)) THEN
     __respuesta:=7;     --Color no elegido
--VALIDACIONES OK. SE PROCEDER� A EFECTUAR LAS INSERCIONES O RECHAZOS DEPENDIENDO:
ELSIF (SELECT chat.fn_chat_consultapersona(_rut)) THEN
    --INSERCI�N DE MENSAJE Y DE LA PERSONA PORQUE RUT NO EXISTE EN LA BD
    --INSERT DE LA PERSONA
    INSERT INTO chat.Persona (nombreapellido, rut, email) VALUES (_nombre, _rut, _correo);
    --INSERTAR EL MENSAJE
    __idpersona := (SELECT idpersona FROM chat.Persona WHERE rut= _rut LIMIT 1);
    INSERT INTO chat.Chat (idpersona, alias, idciudad, mensaje, idcolor) VALUES (__idpersona, _alias, _ciudad, _mensaje, _color);
    __respuesta:=0;
ELSIF (SELECT chat.fn_chat_consultaalias(_rut, _alias)) THEN
    --ALIAS DISPONIBLE Y RUT EXISTENTE. SE ALMACENA EL MENSAJE PERO SE ACTUALIZA LA PERSONA
    UPDATE chat.Persona SET nombreapellido = _nombre, email = _correo WHERE rut= _rut;
    --INSERTAR EL MENSAJE
    __idpersona := (SELECT idpersona FROM chat.Persona WHERE rut= _rut LIMIT 1);
    INSERT INTO chat.Chat (idpersona, alias, idciudad, mensaje, idcolor) VALUES (__idpersona, _alias, _ciudad, _mensaje, _color);
    __respuesta:=0;
ELSE
    __respuesta:=8;     --Nick en uso para el mismo RUT o persona...
END IF;
RETURN __respuesta;
END;
$body$ LANGUAGE 'plpgsql';
--Ejemplo: SELECT chat.fn_chat_iu('Pepsi Light', 'goku77', '8666777-0', 'pepsi@correo.com', 1, 2, 2, 'pepsicola');
--(Orden de inserci�n: nombreapellido, alias, rut, email, pais, ciudad, color, mensaje )
/*SOLO PARA TESTEO DE C�DIGO*/

--Referencias de inserci�n de ejemplo
-- INSERT INTO chat.Persona(nombreapellido, rut, email) VALUES
-- ('Carlos Orrego', '15469832-9', 'yo@mail.com'),
-- ('Queso queso', '15469832-9', 'tu@tumail.com'),
-- ('Asd dsa', '20901792-K', 'NYAN@tumail.com');

-- INSERT INTO chat.Chat(idpersona, alias, idciudad, mensaje, idcolor) VALUES
-- (1,'goku',1,'hola mundo',2),
-- (1,'lelo',1,'hola yokasknkx',2),
-- (2,'goku',5,'soy yop',3),
-- (1,'goku',1,'hola mundo',1),
-- (2,'cacaroto',2,'hola mundo',2),
-- (1,'goku',5,'hola mundo',3),
-- (3,'nyancat',6,'que onda mundo',3),
-- (3,'pepitopagadoble',1,'probando esto',2);



































