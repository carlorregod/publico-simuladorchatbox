//Creando el objeto de validaciones
var valida = new validar();

/* CHECK DE SELECCI�N DE COLORES */
validarojo = function(){ return valida.revisar_ckeck_rojo()};
validanegro = function() {return valida.revisar_ckeck_negro()};
validaverde = function() {return valida.revisar_ckeck_verde()};

/* EJECUCI�N DE LAS OEPRACIONES AL PRESIONAR EL  BOT�N RESPECTIVO */
//Enviar mensaje del chat
function enviar()
{
    //Recolecci�n de variables desde el HTML
    var nombre=document.getElementById('nombre').value;
    var alias=document.getElementById('alias').value;
    var rut=document.getElementById('rut').value;
    var email=document.getElementById('email').value;
    var mensaje=document.getElementById('mensaje').value;
    //Recolecci�n de marcas desde los combobox
    var pais=document.getElementById('pais').value;
    var ciudad=document.getElementById('ciudad').value;
    //Recolecci�n desde los checkbox
    var rojo=document.getElementById('colorrojo').checked;
    var negro=document.getElementById('colornegro').checked;
    var verde=document.getElementById('colorverde').checked;

    /*  CONJUNTO DE REVISIONES ANTES DE ENVIAR DATOS */

    //Validaciones de campos vac�os 
    if(nombre =='' || alias =='' || rut =='' || email =='' || pais== 0 || ciudad== 0)
	{
        alert('Debe completar todos los campos de este formulario del chat.');
        return false;
    }

    //Validando que el nombre y apellido sea ingresado
    if(!valida.revisaNombreApellido(nombre))
    {
        alert('Debe seleccionar al menos su nombre y apellido. Revise de no tener espacios al inicio del ingreso.');
        return false;
    }

    //Validando el Alias
    if(!valida.revisaAlias(alias))
        return false;

    //Chequear que el RUT sea el v�lido
    var checkrut=valida.revisaRut(rut);
    if (checkrut ==2)
    {
        alert("RUT no v�lido. Favor corregir ingreso y/o d�gito verificador de tipo 1111111-1");
        return false;
    }
    else if(checkrut == 1)
    {
        alert("RUT no v�lido. Favor corregir a la forma tipo 1111111-1");
        return false;
    }
    else if(checkrut == 0)
    {
        rut=document.getElementById('rut').value;
    }
    else
    {
        alert('Error.');
    }

    //Efectuar validaci�n de correo electr�nico
    if(!valida.revisaEmail(email))
    {
        alert('Ingrese un correo de forma correcta con formato alguien1@ejemplo.cl.');
        return false;
    }
    //Revisiones de checkbox
    if(!valida.revisarCheckColor())
    {
        alert('Debe seleccionar al menos 1 color dentro de las casillas de color.');
        return false;
    }

    //Revisi�n de los mensajes
    if(!valida.mensajeChat(mensaje))
    {
        alert('Escriba alg�n mensaje...');
        return false;
    }

    //Pasadas todas las validaciones, se precisa a enviar:

    //En la BBDD el color ser� asociado como un "integer" por ende, se le asociar� esta clave de colores
    //1:rojo
    //2:negro
    //3:verde
    var color =0;
    if(rojo)
        color=1;
    else if(negro)
        color=2;
    else if(verde)
        color=3;
    else
        color=0; //Para manejar posibles errores

    // AJAX->Se emplear� una funci�n callback de nombre: fn_callback_envio, ubicada en la zona superior de este archivo
    //Empaquetamiento de variables
    parametro = '&cmd=EnviarDatos'
    +'&nombre='     +nombre
    +'&alias='      +alias
    +'&rut='        +rut
    +'&email='      +email
    +'&mensaje='    +mensaje
    +'&pais='       +pais
    +'&ciudad='     +ciudad
    +'&color='      +color;
    method = "POST"; //Puede ser "GET", "POST" o "REQUEST"
    url    = "php/command.php"; //La url a comunicar
    
    ajaxCallback(parametro, method, url, fn_callback_envio); //Retornar� la respuesta de la funci�n callback definida en fn_callback    
    return false;                   //Fin de la inserci�n
} 

//Generar archivo XML
function descargarXML()
{
    parametro = '&cmd=GenerarXML';
    method = "POST"; //Puede ser "GET", "POST" o "REQUEST"
    url    = "php/command.php"; //La url a comunicar
    ajaxCallback(parametro, method, url, fn_callback_generarXML); //Retornar� la respuesta de la funci�n callback definida en fn_callback    
    return false;
}

//Eliminar un registro
function eliminar(e)
//Par�metro "e" que necesita la funci�n toma el valor de "this" desde el html y genera objetos de sus etiquetas html, entre ellas, �la id y m�s adelante el value!
{
    if(confirm('�Est� seguro que desea borrar la conversaci�n? Esto no puede ser revertido'))
    {
        var id_chat=e.name; //ID del chat seg�n BD
        var id_boton = e.id; //Posici�n del registro seg�n despliegue en formulario
        var totalreg = e.value; //Total del registro
        if(id_boton == totalreg)
        {
            //Protocolo de BORRADO
            var borrar='borrar';
        }
        else
        {
            var borrar='no_borrar';
            //Protocolo de setear eliminado=true
        }
        //Empaquetando para el AJAX
        params='&cmd=BorrarRegistro'
                +'&idchat='     +id_chat
                +'&borrado='    +borrar;
        method = "POST"; //Puede ser "GET", "POST" o "REQUEST"
        url    = "php/command.php"; //La url a comunicar
        ajaxCallback(params, method, url, fn_callbackBorrar); //Retornar� la respuesta de la funci�n callback definida en fn_callback
        //document.getElementById('tablaConversacionesChat').deleteRow(id_boton);
        return false;
    }
}
