/*----INICIO Control de formulario----*/
function soloLetrasNombre(e) 
{
   key = e.keyCode || e.which;
   tecla = String.fromCharCode(key).toLowerCase();
   letras = "�����abcdefghijklmn�opqrstuvwxyz ";  //notar que no dejar� ingreso de espacios. S�lo caracteres
   //especiales = [8,37,39,46];

   tecla_especial = false;

	if(letras.indexOf(tecla)==-1 && !tecla_especial)
	{
		return false;
	}
}

function soloNumerosLetras(e)
{
    //S�lo aceptar� n�meros positivos y decimales
	tecla = (document.all) ? e.keyCode : e.which; 
	if (tecla==8) return true; 
	patron =/^[a-zA-Z0-9��]+$/;
	te = String.fromCharCode(tecla); 
	return patron.test(te);
}

function soloNumerosRut(e)
{
    //S�lo acepta n�meros positivos y decimales
	tecla = (document.all) ? e.keyCode : e.which; 
	if (tecla==8) return true; 
	patron =/^[0-9\-kK]+$/;//este acepta giones y letra k, si se quiere eliminar borrar el punto despues del 9. 
	te = String.fromCharCode(tecla); 
	return patron.test(te);
}

function ingresoCorreoElectronico(e)
{
   key = e.keyCode || e.which;
   tecla = String.fromCharCode(key).toLowerCase();
   letras = "�����abcdefghijklmn�opqrstuvwxyz@._-0123456789"; 

   tecla_especial = false;

	if(letras.indexOf(tecla)==-1 && !tecla_especial)
	{
		return false;
	}
}
/*---FIN CONTROL DE FORMULARIO----*/
/*--- INICIO Validaciones Auxiliares----*/
//Encapsulamiento de las validaciones internas:
var validar = function()
{
    /*****ATRIBUTOS Y METODOS PRIVATIVOS *****/

    //Validar� el nombre y apellido ingresado. M�nimo dos palabras separadas por un espacio
    _revisaNombreApellido = function(nombre)
    {
        //Variable regex permitir� almacenar criterio de validaci�n: Hola Mundo.
        var regex = /^([a-zA-Z������������])+[\ ]{1}([a-zA-Z������������])+/;
        return regex.test(nombre) ? true : false;
    };

    //Revisa el alias ingresado
    _revisaAlias = function(alias)
    {
        //Consultar si el caracter tiene d�gitos
        var regex_num = /\d+/;
        //Consultar si el caracter tiene letras
        var regex_txt= /([a-zAZ��])+/;
        if(alias.search(regex_num) == -1 || alias.search(regex_txt) == -1)
        {
            alert("Alias debe poseer al menos, una letra y/o un n�mero. Vuelva a ingresar.")
            return false;
        }
        //Consulta si la cadena posee al menos, 5 l�neas de extensi�n
        if(alias.length<=5)
        {
            alert("Alias debe poseer m�s de 5 caracteres. N�mero y letras obligatorio.")
            return false;
        }
        //Caso de �xito retornar� un verdadero...
        return true;
    };

    //Validar� el email
    _revisaEmail = function(email)
    {
        //Variable regex permitir� almacenar criterio de validaci�n: nombre1@ejemplo.com.
        var regex = /^([a-zA-Z])([\w.-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email) ? true : false;
    };

    //Validar� el RUT ingresado
    _revisaRut = function(rut)
    {
        // Despejar Puntos
        var valor = rut.replace('.','');
        // Despejar Gui�n
        valor = valor.replace('-','');
        // Aislar Cuerpo y D�gito Verificador
        cuerpo = valor.slice(0,-1);
        dv = valor.slice(-1).toUpperCase();
        // Formatear RUN
        rut.value = cuerpo + '-'+ dv
        // Si no cumple con el m�nimo ej. (n.nnn.nnn)
        if(cuerpo.length < 7)
        {
            return 1; //Equivale a RUT inv�lido por ingreso
        }
        // Calcular d�gito verificador
        suma = 0;
        multiplo = 2;
        // Para cada d�gito del Cuerpo
        for(i=1;i<=cuerpo.length;i++)
        {
            // Obtener su Producto con el M�ltiplo Correspondiente
            index = multiplo * valor.charAt(cuerpo.length - i);
            // Sumar al Contador General
            suma = suma + index;
            // Consolidar M�ltiplo dentro del rango [2,7]
            if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
        }
        // Calcular D�gito Verificador en base al M�dulo 11
        dvEsperado = 11 - (suma % 11);
        // Casos Especiales (0 y K)
        dv = (dv == 'K' || dv == 'k')?10:dv;
        dv = (dv == 0)?11:dv;
        // Validar que el Cuerpo coincide con su D�gito Verificador
        if(dvEsperado != dv) 
            return 2; //nEquivale a RUT inv�lido por nv invalido
        //Retorno al d�gito verificador
        dv = (dv == 10)?'K':dv;
        dv = (dv == 11)?0:dv;
        document.getElementById('rut').value = cuerpo+"-"+dv;
        return 0; //Equivale a que el RUT es v�lido
    };

    //Validaci�n de checkbox: S�lo 1 debe ser true. Alternativas de no selecci�n no permitidas
    _revisar_ckeck_rojo = function()
    {
        document.getElementById('colornegro').checked=false;
        document.getElementById('colorverde').checked=false;
        document.getElementById('mensaje').style.color='#F80000';
    };
    _revisar_ckeck_negro = function()
    {
        document.getElementById('colorrojo').checked=false;
        document.getElementById('colorverde').checked=false;
        document.getElementById('mensaje').style.color='#000000';
    };
    _revisar_ckeck_verde = function()
    {
        document.getElementById('colorrojo').checked=false;
        document.getElementById('colornegro').checked=false;
        document.getElementById('mensaje').style.color='#AEA04B';
    };

    //Se asegurar� de revisar que exista aunque sea, un check de color
    _revisarCheckColor = function()
    {
        var contar=0;
        if(document.getElementById('colorrojo').checked)
            return true;
        else if(document.getElementById('colornegro').checked)
            return true;
        else if(document.getElementById('colorverde').checked)
            return true;
        else
            return false;
    };

    //�Existe alg�n mensaje en el chatbox?
    _mensajeChat = function(mensaje)
    {
        if(mensaje === null || mensaje === "")
            return false;  
        else
            return true; 
    };

    /*****ATRIBUTOS Y METODOS PUBLICOS->RETURN *****/

    //Llamada a funci�n nombre-apellido
    this.revisaNombreApellido = function(n)
    {
        return _revisaNombreApellido(n);
    };

    //Llamada a funci�n alias
    this.revisaAlias = function(a)
    {
        return _revisaAlias(a);
    };

    //Llamada a funci�n email
    this.revisaEmail = function(correo)
    {
        return _revisaEmail(correo);
    };

    //Llamada a funci�n rut
    this.revisaRut = function(r)
    {
        return _revisaRut(r);
    };

    //Llamada a funciones de chequeos de checkbox
    this.revisar_ckeck_rojo = function()
    {
        return _revisar_ckeck_rojo();
    };
    this.revisar_ckeck_negro = function()
    {
        return _revisar_ckeck_negro();
    };
    this.revisar_ckeck_verde = function()
    {
        return _revisar_ckeck_verde();
    };

    //Llamada a funci�n que se cerciora de elegir un checkbox de color
    this.revisarCheckColor = function()
    {
        return _revisarCheckColor();
    };

    //Llamada a funci�n de mensaje de chat
    this.mensajeChat = function(msg)
    {
        return _mensajeChat(msg);
    };
};

/*--- FIN Validaciones Auxiliares----*/
/*----INICIO c�psula ajax---*/

/*AJAX Nativo- Seguir este orden para que funcione*/
//Funci�n para llamar al ajax. Se puede reutilizar cambiando argumentos, los callbacks son particulares
function ajaxCallback(params, method, url, callback, asynchr=true )
{
	var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
	xhttp.onreadystatechange = function() {
		if (this.readyState === 4 && this.status === 200) 
		{
			var resp = xhttp.responseText;
			//var respJson = JSON.parse(resp);
			callback(resp);
		}
		else
		{
			console.log("xhr ha fallado...");
		}
	};
	xhttp.open(method, url, asynchr);    // Tipo de comunicacion
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send(params);  // envio de http
	console.log("Enviando petici�n al servidor...");
	//method: Puede ser GET, POST, REQUEST,
	//params: los par�metros empaquetados. Ojo si va por GET este argumento debe declararse como null.
	//url: El url a donde se comunicar�, ejemplo: http://php.command.php
	//asynchr: Es opcional, por defecto es true (as�ncrono), sise desea que sea s�ncrono, que sea false.
	//Ejemplo: ajaxPOST(null, "GET","php/command.php", fn_callback)
	//la funci�n callback o mejor dicho, fn_callback se trabajar� ahora
	return false;
}

/*----FIN c�psula ajax---*/
/***** INICIO-COLECCI�N DE CALLBACK *****/
//Funci�n Callback para generar el combobox de los pa�ses
fn_callbackPais = function(data)
{
	document.getElementById('pais').innerHTML = data;
	document.getElementById('ciudad').innerHTML='<option value="0" id="0">Seleccionar</option>';
};

 //Funci�n Callback para generar el combobox de las ciudades
fn_callbackCiudad = function(data)
{
	document.getElementById('ciudad').innerHTML = data;
};

//Funci�n callback: usuarios conectados
fn_callbackUserConectados = function(data)
{
	document.getElementById('usuariosConectados').innerHTML = data;
}

//Funci�n callback: panel de conversaicones
fn_callbackPanelConversaciones = function(data)
{
	document.getElementById('conversacionesGuardadas').innerHTML = data;
}

//Callback del eliminado de registros
fn_callbackBorrar = function(eliminado)
{
    if(mensaje='exito') 
    {
		alert('Registro eliminado');
		cargaPanelConversacion();
        cargaUsuariosConectados();
    }
    else
        alert('Error. Int�ntelo m�s adelante');
}

//Callback del env�o de datos
fn_callback_envio = function(data)
{
	switch(data)
	{
		case '-1':  //Error por espacios en blanco
					alert('Debe completar todos los campos del formulario.');
					console.log(data); 
					break;

		case '1': //Error por nombre
					alert('Debe seleccionar al menos su nombre y apellido. Revise de no tener espacios al inicio del ingreso. ');
					console.log(data); 
					break;

		case '2': //Error por alias no tiene letras ni n�meros
					alert("Alias debe poseer al menos, una letra y/o un n�mero. Vuelva a ingresar.");
					console.log(data); 
					break;

		case '3': //Error por alias no posee el largo m�nimo
					alert("Alias debe poseer m�s de 5 caracteres. N�mero y letras obligatorio.")
					console.log(data); 
					break;

		case '4': //Error por rut
					alert('RUT no v�lido. Favor corregir ingreso y/o d�gito verificador de tipo 1111111-1.');
					console.log(data); 
					break;

		case '5': //Error por email
					alert('Ingrese un correo de forma correcta con formato alguien1@ejemplo.cl.');
					console.log(data); 
					break;

		case '6': //Error por mensaje nulo
					alert('Escriba alg�n mensaje...');
					console.log(data); 
					break;

		case '7': //color no elegido
					alert('Debe seleccionar al menos 1 color dentro de las casillas de color.');
					console.log(data); 
					break;

		case '8': //Nick en uso para ese RUT
					alert('El nick se encuentra empleado para el RUT. Favor especificar otro.');
					console.log(data); 
					break;

		case '9': //Falla de BBDD no hace el insert por try/catch
					alert('Error del sistema. Por favor vuelva a intentar en otro momento.');
					console.log(data); 
					break;

		case '0': //Casu�stica de �xito
					cargaUsuariosConectados();
					cargaPanelConversacion();
					limpiarMensaje();
					console.log(data); 
					break;

		default: //Errores diversos capa 8
					alert('Error')
					console.log('Error desconocido capa 8 ( ?� ?? ?�)'); 
					break;
	}
	
};

fn_callback_generarXML = function(data)
{
	//document.location = 'php/chat.xml'; //XML saca el formulario y se superposiciona
	window.open('php/chat.xml');		//XML se abre en otra pesta�a
};

/***** FIN- DE DECLARACI�N DE FUNCIONES CALLBACK *****/
/*-----INICIO-Funciones auxiliares-----*/
//Limpia mensajes luego de enviar alguno en el chat
function limpiarMensaje()
{
	var mensaje=document.getElementById('mensaje').value="";
}
//Funciones para hacer la carga del pa�s y ciudad respectivamente
function cargaPais()
{
    params = '&cmd=cmbPais';
    method = "POST"; //Puede ser "GET", "POST" o "REQUEST"
    url    = "php/command.php"; //La url a comunicar
    ajaxCallback(params, method, url, fn_callbackPais); //Retornar� la respuesta de la funci�n callback definida en fn_callback
    return false;
}
function cargaCiudad()
{
    var pais= document.getElementById('pais').value;
    params = '&cmd=cmbCiudad'
            +'&id_pais='    +pais;
    method = "POST"; //Puede ser "GET", "POST" o "REQUEST"
    url    = "php/command.php"; //La url a comunicar
    ajaxCallback(params, method, url, fn_callbackCiudad); //Retornar� la respuesta de la funci�n callback definida en fn_callback
    return false;
}
//Cargando la tabla de usuarios-conectados
function cargaUsuariosConectados()
{
	params = '&cmd=lstUserConec'
    method = "POST"; //Puede ser "GET", "POST" o "REQUEST"
    url    = "php/command.php"; //La url a comunicar
    ajaxCallback(params, method, url, fn_callbackUserConectados); //Retornar� la respuesta de la funci�n callback definida en fn_callback
    return false;
}

//Cargando la tabla de usuarios-conectados
function cargaPanelConversacion()
{
	params = '&cmd=lstConversacion'
    method = "POST"; //Puede ser "GET", "POST" o "REQUEST"
    url    = "php/command.php"; //La url a comunicar
    ajaxCallback(params, method, url, fn_callbackPanelConversaciones); //Retornar� la respuesta de la funci�n callback definida en fn_callback
    return false;
}
/*-----FIN-Funciones auxiliares-----*/
/*-----INICIO-Funciones de evento-----*/
//Usando el ajax para generar el combobox pa�s en cuanto cargue la p�gina
window.onload = function()
{
    document.getElementById('colorrojo').checked=false;
    document.getElementById('colorverde').checked=false;
    document.getElementById('colornegro').checked=false;
	cargaPais();
	cargaUsuariosConectados();
	cargaPanelConversacion();
    return false;
}
//Usando el ajax para generar el combobox de ciudad s�lo si existe una selecci�n previa del pa�s
document.getElementById('pais').onchange = function()
{
    cargaCiudad();
    return false;
}
/*-----FIN-Funciones de evento-----*/



