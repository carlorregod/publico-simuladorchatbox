/*RESPALDO PRE-ENCAPSULAMIENTO*/
/*----PARTE 1: REVISI�N DE VALIDACIONES PREVIAS A LA INSERCI�N----*/

/*PARTE 1: M�TODOS Y FUNCIONES VARIAS*/
//Revisi�n de ingresos Nombre, alias, RUT y email. Si aceptan las condiciones espec�ficas:

//Validar� el nombre y apellido ingresado. M�nimo dos palabras separadas por un espacio
function revisaNombreApellido(nombre)
{
    //Variable regex permitir� almacenar criterio de validaci�n: Hola Mundo.
    var regex = /^([a-zA-Z������������])+[\ ]{1}([a-zA-Z������������])+/;
    return regex.test(nombre) ? true : false;
}

//Revisa el alias ingresado
function revisaAlias(alias)
{
    //Consultar si el caracter tiene d�gitos
    var regex_num = /\d+/;
    //Consultar si el caracter tiene letras
    var regex_txt= /([a-zAZ��])+/;
    if(alias.search(regex_num) == -1 || alias.search(regex_txt) == -1)
    {
        alert("Alias debe poseer al menos, una letra y/o un n�mero. Vuelva a ingresar.")
        return false;
    }
    //Consulta si la cadena posee al menos, 5 l�neas de extensi�n
    if(alias.length<=5)
    {
        alert("Alias debe poseer m�s de 5 caracteres. N�mero y letras obligatorio.")
        return false;
    }
    //Caso de �xito retornar� un verdadero...
    return true;
}

//Validar� el RUT ingresado
function revisaRut(rut)
{
    // Despejar Puntos
    var valor = rut.replace('.','');
    // Despejar Gui�n
    valor = valor.replace('-','');
    // Aislar Cuerpo y D�gito Verificador
    cuerpo = valor.slice(0,-1);
    dv = valor.slice(-1).toUpperCase();
    // Formatear RUN
    rut.value = cuerpo + '-'+ dv
    // Si no cumple con el m�nimo ej. (n.nnn.nnn)
    if(cuerpo.length < 7)
    {
        return 1; //Equivale a RUT inv�lido por ingreso
    }
    // Calcular d�gito verificador
    suma = 0;
    multiplo = 2;
    // Para cada d�gito del Cuerpo
    for(i=1;i<=cuerpo.length;i++)
   {
        // Obtener su Producto con el M�ltiplo Correspondiente
        index = multiplo * valor.charAt(cuerpo.length - i);
        // Sumar al Contador General
        suma = suma + index;
        // Consolidar M�ltiplo dentro del rango [2,7]
        if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
    }

    // Calcular D�gito Verificador en base al M�dulo 11
    dvEsperado = 11 - (suma % 11);

    // Casos Especiales (0 y K)
    dv = (dv == 'K' || dv == 'k')?10:dv;
    dv = (dv == 0)?11:dv;

    // Validar que el Cuerpo coincide con su D�gito Verificador
    if(dvEsperado != dv) 
        return 2; //nEquivale a RUT inv�lido por nv invalido
    //Retorno al d�gito verificador
    dv = (dv == 10)?'K':dv;
    dv = (dv == 11)?0:dv;
    document.getElementById('rut').value = cuerpo+"-"+dv;
    return 0; //Equivale a que el RUT es v�lido
}

//Validar� el email
function revisaEmail(email)
{
    //Variable regex permitir� almacenar criterio de validaci�n: nombre1@ejemplo.com.
    var regex = /^([a-zA-Z])([\w.-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email) ? true : false;
}

//Validaci�n de checkbox: S�lo 1 debe ser true. Alternativas de no selecci�n no permitidas
function revisar_ckeck_rojo()
{
    document.getElementById('colornegro').checked=false;
    document.getElementById('colorverde').checked=false;
    document.getElementById('mensaje').style.color='#F80000';
}
function revisar_ckeck_negro()
{
    document.getElementById('colorrojo').checked=false;
    document.getElementById('colorverde').checked=false;
    document.getElementById('mensaje').style.color='#000000';
}

function revisar_ckeck_verde()
{
    document.getElementById('colorrojo').checked=false;
    document.getElementById('colornegro').checked=false;
    document.getElementById('mensaje').style.color='#AEA04B';
}

//Se asegurar� de revisar que exista aunque sea, un check de color
function revisarCheckColor()
{
    var contar=0;
    if(document.getElementById('colorrojo').checked)
        return true;
    else if(document.getElementById('colornegro').checked)
        return true;
    else if(document.getElementById('colorverde').checked)
        return true;
    else
        return false;
}

//�Existe alg�n mensaje en el chatbox?
function mensajeChat(mensaje)
{
    if(mensaje === null || mensaje === "")
        return false;  
    else
        return true; 
}

/*FIN PARTE 1*/

/*PARTE 2: preparar los callbacks de retorno de Ajax

/*LAS OEPRACIONES POR BOT�N: PARTE 2 */
function enviar()
{
    //Recolecci�n de variables desde el HTML
    var nombre=document.getElementById('nombre').value;
    var alias=document.getElementById('alias').value;
    var rut=document.getElementById('rut').value;
    var email=document.getElementById('email').value;
    var mensaje=document.getElementById('mensaje').value;
    //Recolecci�n de marcas desde los combobox
    var pais=document.getElementById('pais').value;
    var ciudad=document.getElementById('ciudad').value;
    //Recolecci�n desde los checkbox
    var rojo=document.getElementById('colorrojo').checked;
    var negro=document.getElementById('colornegro').checked;
    var verde=document.getElementById('colorverde').checked;

    /*  CONJUNTO DE REVISIONES ANTES DE ENVIAR DATOS */

    //Validaciones de campos vac�os 
    if(nombre =='' || alias =='' || rut =='' || email =='' || pais== 0 || ciudad== 0)
	{
        alert('Debe completar todos los campos de este formulario del chat.');
        return false;
    }

    //Validando que el nombre y apellido sea ingresado
    if(!revisaNombreApellido(nombre))
    {
        alert('Debe seleccionar al menos su nombre y apellido. Revise de no tener espacios al inicio del ingreso.');
        return false;
    }

    //Validando el Alias
    if(!revisaAlias(alias))
        return false;

    //Chequear que el RUT sea el v�lido
    var checkrut=revisaRut(rut);
    if (checkrut ==2)
    {
        alert("RUT no v�lido. Favor corregir ingreso y/o d�gito verificador de tipo 1111111-1");
        return false;
    }
    else if(checkrut == 1)
    {
        alert("RUT no v�lido. Favor corregir a la forma tipo 1111111-1");
        return false;
    }
    else if(checkrut == 0)
    {
        rut=document.getElementById('rut').value;
    }
    else
    {
        alert('Error.');
    }

    //Efectuar validaci�n de correo electr�nico
    if(!revisaEmail(email))
    {
        alert('Ingrese un correo de forma correcta con formato alguien1@ejemplo.cl.');
        return false;
    }
    //Revisiones de checkbox
    if(!revisarCheckColor())
    {
        alert('Debe seleccionar al menos 1 color dentro de las casillas de color.');
        return false;
    }

    //Revisi�n de los mensajes
    if(!mensajeChat(mensaje))
    {
        alert('Escriba alg�n mensaje...');
        return false;
    }

    //Pasadas todas las validaciones, se precisa a enviar:

    //En la BBDD el color ser� asociado como un "integer" por ende, se le asociar� esta clave de colores
    //1:rojo
    //2:negro
    //3:verde
    var color =0;
    if(rojo)
        color=1;
    else if(negro)
        color=2;
    else if(verde)
        color=3;
    else
        color=0; //Para manejar posibles errores

    // AJAX->Se emplear� una funci�n callback de nombre: fn_callback_envio, ubicada en la zona superior de este archivo
    //Empaquetamiento de variables
    parametro = '&cmd=EnviarDatos'
    +'&nombre='     +nombre
    +'&alias='      +alias
    +'&rut='        +rut
    +'&email='      +email
    +'&mensaje='    +mensaje
    +'&pais='       +pais
    +'&ciudad='     +ciudad
    +'&color='      +color;
    method = "POST"; //Puede ser "GET", "POST" o "REQUEST"
    url    = "php/command.php"; //La url a comunicar
    
    ajaxCallback(parametro, method, url, fn_callback_envio); //Retornar� la respuesta de la funci�n callback definida en fn_callback    
    return false;                   //Fin de la inserci�n
} 


function descargarXML()
{
    parametro = '&cmd=GenerarXML';
    method = "POST"; //Puede ser "GET", "POST" o "REQUEST"
    url    = "php/command.php"; //La url a comunicar
    ajaxCallback(parametro, method, url, fn_generarXML); //Retornar� la respuesta de la funci�n callback definida en fn_callback    
    return false;
}

/*Fin operaciones por bot�n */

/*PARTE 4: ACCI�N DE DELETE*/
//Par�metro "e" que necesita la funci�n toma el valor de "this" desde el html y genera objetos de sus etiquetas html, entre ellas, �la id y m�s adelante el value!

function eliminar(e)
{
    if(confirm('�Est� seguro que desea borrar la conversaci�n? Esto no puede ser revertido'))
    {
        var id_chat=e.name; //ID del chat seg�n BD
        var id_boton = e.id; //Posici�n del registro seg�n despliegue en formulario
        var totalreg = e.value; //Total del registro
        if(id_boton == totalreg)
        {
            //Protocolo de BORRADO
            var borrar='borrar';
        }
        else
        {
            var borrar='no_borrar';
            //Protocolo de setear eliminado=true
        }
        //Empaquetando para el AJAX
        params='&cmd=BorrarRegistro'
                +'&idchat='     +id_chat
                +'&borrado='    +borrar;
        method = "POST"; //Puede ser "GET", "POST" o "REQUEST"
        url    = "php/command.php"; //La url a comunicar
        ajaxCallback(params, method, url, fn_callbackBorrar); //Retornar� la respuesta de la funci�n callback definida en fn_callback
        //document.getElementById('tablaConversacionesChat').deleteRow(id_boton);
        return false;
    }
}
