/*----INICIO Control de formulario----*/

function soloLetrasNombre(e) 
{
   key = e.keyCode || e.which;
   tecla = String.fromCharCode(key).toLowerCase();
   letras = "�����abcdefghijklmn�opqrstuvwxyz ";  //notar que no dejar� ingreso de espacios. S�lo caracteres
   //especiales = [8,37,39,46];

   tecla_especial = false;

	if(letras.indexOf(tecla)==-1 && !tecla_especial)
	{
		return false;
	}
}

function soloNumerosLetras(e)
{
    //S�lo aceptar� n�meros positivos y decimales
	tecla = (document.all) ? e.keyCode : e.which; 
	if (tecla==8) return true; 
	patron =/^[a-zA-Z0-9��]+$/;
	te = String.fromCharCode(tecla); 
	return patron.test(te);
}

function soloNumerosRut(e)
{
    //S�lo acepta n�meros positivos y decimales
	tecla = (document.all) ? e.keyCode : e.which; 
	if (tecla==8) return true; 
	patron =/^[0-9\-kK]+$/;//este acepta giones y letra k, si se quiere eliminar borrar el punto despues del 9. 
	te = String.fromCharCode(tecla); 
	return patron.test(te);
}

function ingresoCorreoElectronico(e)
{
   key = e.keyCode || e.which;
   tecla = String.fromCharCode(key).toLowerCase();
   letras = "�����abcdefghijklmn�opqrstuvwxyz@._-0123456789"; 

   tecla_especial = false;

	if(letras.indexOf(tecla)==-1 && !tecla_especial)
	{
		return false;
	}
}
/*---FIN CONTROL DE FORMULARIO----*/

/*--Funciones auxiliares--*/
function limpiarMensaje()
{
	var mensaje=document.getElementById('mensaje').value="";
}

/*----INICIO Efectuar los combobox y ajax---*/

/*AJAX Nativo- Seguir este orden para que funcione*/
//1.- Funci�n para llamar al ajax. Se puede reutilizar cambiando argumentos
function ajaxCallback(params, method, url, callback, asynchr=true )
{
	var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
	xhttp.onreadystatechange = function() {
		if (this.readyState === 4 && this.status === 200) 
		{
			var resp = xhttp.responseText;
			//var respJson = JSON.parse(resp);
			callback(resp);
		}
		else
		{
			console.log("xhr ha fallado...");
		}
	};
	xhttp.open(method, url, asynchr);    // Tipo de comunicacion
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send(params);  // envio de http
	console.log("Enviando petici�n al servidor...");
	//method: Puede ser GET, POST, REQUEST,
	//params: los par�metros empaquetados. Ojo si va por GET este argumento debe declararse como null.
	//url: El url a donde se comunicar�, ejemplo: http://php.command.php
	//asynchr: Es opcional, por defecto es true (as�ncrono), sise desea que sea s�ncrono, que sea false.
	//Ejemplo: ajaxPOST(null, "GET","php/command.php", fn_callback)
	//la funci�n callback o mejor dicho, fn_callback se trabajar� ahora
	return false;
}

//Funci�n Callback para generar el combobox de los pa�ses
fn_callbackPais = function(data)
{
	document.getElementById('pais').innerHTML = data;
	document.getElementById('ciudad').innerHTML='<option value="0" id="0">Seleccionar</option>';
};

 //Funci�n Callback para generar el combobox de las ciudades
fn_callbackCiudad = function(data)
{
	document.getElementById('ciudad').innerHTML = data;
};

//Funci�n callback: usuarios conectados
fn_callbackUserConectados = function(data)
{
	document.getElementById('usuariosConectados').innerHTML = data;
}

//Funci�n callback: panel de conversaicones
fn_callbackPanelConversaciones = function(data)
{
	document.getElementById('conversacionesGuardadas').innerHTML = data;
}

//Funciones para hacer la carga del pa�s y ciudad respectivamente
function cargaPais()
{
    params = '&cmd=cmbPais';
    method = "POST"; //Puede ser "GET", "POST" o "REQUEST"
    url    = "php/command.php"; //La url a comunicar
    ajaxCallback(params, method, url, fn_callbackPais); //Retornar� la respuesta de la funci�n callback definida en fn_callback
    return false;
}

function cargaCiudad()
{
    var pais= document.getElementById('pais').value;
    params = '&cmd=cmbCiudad'
            +'&id_pais='    +pais;
    method = "POST"; //Puede ser "GET", "POST" o "REQUEST"
    url    = "php/command.php"; //La url a comunicar
    ajaxCallback(params, method, url, fn_callbackCiudad); //Retornar� la respuesta de la funci�n callback definida en fn_callback
    return false;
}

//Cargando la tabla de usuarios-conectados
function cargaUsuariosConectados()
{
	params = '&cmd=lstUserConec'
    method = "POST"; //Puede ser "GET", "POST" o "REQUEST"
    url    = "php/command.php"; //La url a comunicar
    ajaxCallback(params, method, url, fn_callbackUserConectados); //Retornar� la respuesta de la funci�n callback definida en fn_callback
    return false;
}

//Cargando la tabla de usuarios-conectados
function cargaPanelConversacion()
{
	params = '&cmd=lstConversacion'
    method = "POST"; //Puede ser "GET", "POST" o "REQUEST"
    url    = "php/command.php"; //La url a comunicar
    ajaxCallback(params, method, url, fn_callbackPanelConversaciones); //Retornar� la respuesta de la funci�n callback definida en fn_callback
    return false;
}

//Usando el ajax para generar el combobox pa�s en cuanto cargue la p�gina
window.onload = function()
{
    document.getElementById('colorrojo').checked=false;
    document.getElementById('colorverde').checked=false;
    document.getElementById('colornegro').checked=false;
	cargaPais();
	cargaUsuariosConectados();
	cargaPanelConversacion();
    return false;
}

//Usando el ajax para generar el combobox de ciudad s�lo si existe una selecci�n previa del pa�s
document.getElementById('pais').onchange = function()
{
    cargaCiudad();
    return false;
}

//Callback del eliminado de registros
fn_callbackBorrar = function(eliminado)
{
    if(mensaje='exito') 
    {
		alert('Registro eliminado');
		cargaPanelConversacion();
        cargaUsuariosConectados();
    }
    else
        alert('Error. Int�ntelo m�s adelante');
}

//Callback del env�o de datos
fn_callback_envio = function(data)
{
	switch(data)
	{
		case '-1':  //Error por espacios en blanco
					alert('Debe completar todos los campos del formulario.');
					console.log(data); 
					break;

		case '1': //Error por nombre
					alert('Debe seleccionar al menos su nombre y apellido. Revise de no tener espacios al inicio del ingreso. ');
					console.log(data); 
					break;

		case '2': //Error por alias no tiene letras ni n�meros
					alert("Alias debe poseer al menos, una letra y/o un n�mero. Vuelva a ingresar.");
					console.log(data); 
					break;

		case '3': //Error por alias no posee el largo m�nimo
					alert("Alias debe poseer m�s de 5 caracteres. N�mero y letras obligatorio.")
					console.log(data); 
					break;

		case '4': //Error por rut
					alert('RUT no v�lido. Favor corregir ingreso y/o d�gito verificador de tipo 1111111-1.');
					console.log(data); 
					break;

		case '5': //Error por email
					alert('Ingrese un correo de forma correcta con formato alguien1@ejemplo.cl.');
					console.log(data); 
					break;

		case '6': //Error por mensaje nulo
					alert('Escriba alg�n mensaje...');
					console.log(data); 
					break;

		case '7': //color no elegido
					alert('Debe seleccionar al menos 1 color dentro de las casillas de color.');
					console.log(data); 
					break;

		case '8': //Nick en uso para ese RUT
					alert('El nick se encuentra empleado para el RUT. Favor especificar otro.');
					console.log(data); 
					break;

		case '9': //Falla de BBDD no hace el insert por try/catch
					alert('Error del sistema. Por favor vuelva a intentar en otro momento.');
					console.log(data); 
					break;

		case '0': //Casu�stica de �xito
					cargaUsuariosConectados();
					cargaPanelConversacion();
					limpiarMensaje();
					console.log(data); 
					break;

		default: //Errores diversos capa 8
					alert('Error')
					console.log('Error desconocido capa 8 ( ?� ?? ?�)'); 
					break;
	}
	
};

fn_generarXML = function(data)
{
	//document.location = 'php/chat.xml'; //XML saca el formulario y se superposiciona
	window.open('php/chat.xml');		//XML se abre en otra pesta�a
};