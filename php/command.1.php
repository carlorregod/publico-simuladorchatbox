<?php
//BACKUP DE LOS COMANDOS
require 'conexion.php';

$cmd=$_POST['cmd'];

switch($cmd)
{
    case 'cmbPais': 
        //Combobox del pa�s     
        $pgsql=getConnect();
        $query="SELECT * FROM chat.pais";
        $consulta=$pgsql->prepare($query);
        $consulta->execute();
        $listas='<option value="0" id="0">Seleccionar</option>';
        while ($fila = $consulta->fetchObject())
        {
            $listas .='<option value="'.$fila->idpais.'" id="'.$fila->idpais.'">'.$fila->nombrepais.'</option>';
        } 
        echo $listas;
        break;

    case 'cmbCiudad':
        //Valor del id del pa�s rescatado
        $id_pais = $_POST['id_pais'];
        //Combobox de la ciudad
        $pgsql=getConnect();  
        $query="SELECT * FROM chat.vw_PaisCiudad WHERE idpais='$id_pais'";
        $consulta=$pgsql->prepare($query);
        $consulta->execute();
        $listas='<option value="0" id="0">Seleccionar</option>';
        while ($fila = $consulta->fetchObject())
        {
            $listas .='<option value="'.$fila->idciudad.'" id="'.$fila->idciudad.'">'.$fila->nombreciudad.'</option>';
        } 
        echo $listas;
        break; 

    case 'lstUserConec':
        $pgsql=getConnect();
        $query="SELECT * FROM chat.vw_totalchats";
        $consulta=$pgsql->prepare($query);
        $consulta->execute();
        $listas='';
        while ($fila = $consulta->fetchObject())
        {
            $listas .='<tr><td class="formatoFilasTablaConextados">'
            .$fila->nombrepais.
            '</td><td class="formatoFilasTablaConextados">'
            .$fila->conectados.
            '</td></tr>';
        } 
        echo $listas;
        break;

        case 'lstConversacion':
            $pgsql=getConnect();
            $query="SELECT * FROM chat.vw_listachats";
            $consulta=$pgsql->prepare($query);
            $consulta->execute();
            $cuentaFila = $consulta->rowCount();
            $listas='';
            $i=1; //Para inicializar cantidad de filas
            while ($fila = $consulta->fetchObject())
            {
                $listas .='<tr><td class="formatoFilasTablaConversaciones">>>>'
                .$fila->alias.
                '</td><td class="formatoFilasTablaConversaciones">-</td><td class="formatoFilasTablaConversaciones">'
                .$fila->nombrepais.
                '</td><td class="formatoFilasTablaConversaciones">-</td><td class="formatoFilasTablaConversaciones">'
                .$fila->fechahora.
                '</td><td class="formatoFilasTablaConversaciones">-</td><td class="formatoFilasTablaConversaciones '.$fila->nombrecolor.'">'
                .$fila->mensaje.
                '</td><td class="formatoFilasTablaConversaciones">-</td><td class="formatoFilasTablaConversaciones">
                <button id="'
                .$i.
                '" value="'.$cuentaFila.'" name="'.$fila->idchat.'" class="botonesChat" onclick="eliminar(this)">ELIMINAR</button></td></tr>'; 
                //Aumento de counter
                $i+=1;  
                        
            } 
            echo $listas;
            break;
      
    case 'EnviarDatos': 
        /*Traslado de datos empaquetados*/
        $nombre     =utf8_decode(pg_escape_string($_POST['nombre']));
        $alias      =utf8_decode(pg_escape_string($_POST['alias']));
        $rut        =utf8_decode(pg_escape_string($_POST['rut']));
        $email      =utf8_decode(pg_escape_string($_POST['email']));
        $mensaje    =utf8_decode(pg_escape_string($_POST['mensaje']));
        $pais       =utf8_decode($_POST['pais']);
        $ciudad     =utf8_decode($_POST['ciudad']);
        $color      =utf8_decode($_POST['color']);
        //Conectando a la BD
        $pgsql=getConnect();
        //�El rut existe previamente?
        $query_rut="SELECT chat.fn_chat_consultapersona('$rut')";
        $rut_disponible=$pgsql->query($query_rut)->fetchAll()[0][0]; 
        //Hay que verificar que el RUT no tenga el mismo alias  
        $query_alias="SELECT chat.fn_chat_consultaalias('$rut', '$alias')";
        $c_alias=$pgsql->query($query_alias)->fetchAll()[0][0]; //Captura del booleano
        //Si el alias ya existe, la inserci�n de datos no se realizar�. Cada RUT deber� tener alias distintos
        //Podr�n existir uno o varios RUT
        if(!$rut_disponible && !$c_alias)
        {
            echo '8';
            break;
        }
        else
        {
            try
            {
                //Peque�a validaci�n de la ciudad
                $ciudad = null ? 0 : $ciudad;
                $query = "SELECT chat.fn_chat_iu('$nombre', '$alias', '$rut', '$email', '$pais', '$ciudad', '$color', '$mensaje')";
                $consulta=$pgsql->prepare($query);
                $consulta->execute();
                $respuesta=$consulta->fetchAll()[0][0];
                echo $respuesta;
                break;
            }
            catch(Exception $e)
            {
                echo '9';
                break;
            }
        }

    case 'BorrarRegistro':
        //Recolectando variables
        $idchat=$_POST['idchat'];
        $borrado=$_POST['borrado'];
        //Luego de iniciar la sesi�n, se procede a llamar al m�todo de borrado
        try
        {
            $pgsql=getConnect();
            $query="SELECT chat.fn_chat_d('$idchat','$borrado')";
            $q_cancha= $pgsql->prepare($query);
            $q_cancha->execute();
            echo 'exito';
            break;
        }
        catch(Exception $e)
        {
            echo 'fracaso';
            break;
        }

    case 'GenerarXML':
        //Generando la query para insertar todo dentro de $campo
        $pgsql = getConnect();
        $query = "SELECT * FROM chat.vw_xml";
        $resp = $pgsql->prepare($query);
        $resp ->execute();
        $resultado = $resp->fetchAll() or die('Error');
        $fn = "chat.xml"; //Ruta en donde se guardar� el XML
        header('Content-type: text/xml');
        header("Content-Disposition: attachment; filename=\"" . $fn . "\";" );
        $cuentaFila = $resp->rowCount();  
        //Armando el XML
        if ($cuentaFila > 0) 
        {
            $xml = new DomDocument('1.0', 'UTF-8');
            $xml->formatOutput = true;
            $chat_ = $xml->createElement('Chats');
            $chat_ = $xml->appendChild($chat_);
            foreach($resultado as $filas)
            {
                //NODO PADRE
                $chat = $xml->createElement('chat'); //ETIQUETA
                $chat = $chat_->appendChild($chat);  //REFERENCIA AL NODO PADRE EN DODE SE INCRUSTAR�. chat_ ES EL PADRE

                $persona = $xml->createElement('persona');
                $persona = $chat->appendChild($persona);
                    //NODO HIJO
                    $idchat = $xml->createElement('idchat', $filas['idchat']); //ETIQUETA, VALOR
                    $idchat = $persona->appendChild($idchat);

                    $nombre = $xml->createElement('nombre', $filas['nombreapellido']);
                    $nombre = $persona->appendChild($nombre);

                    $alias = $xml->createElement('alias', $filas['alias']);
                    $alias = $persona->appendChild($alias);

                    $rut = $xml->createElement('rut', $filas['rut']);
                    $rut = $persona->appendChild($rut);

                    $email = $xml->createElement('email', $filas['email']);
                    $email = $persona->appendChild($email);
                //NODO PADRE
                $direccion = $xml->createElement('direccion');
                $direccion = $persona->appendChild($direccion);
                    //NODO HIJO
                    $idpais = $xml->createElement('idpais', $filas['idpais']);
                    $idpais = $direccion->appendChild($idpais);

                    $pais = $xml->createElement('pais', $filas['nombrepais']);
                    $pais = $direccion->appendChild($pais);

                    $idciudad = $xml->createElement('idciudad', $filas['idciudad']);
                    $idciudad = $direccion->appendChild($idciudad);

                    $ciudad = $xml->createElement('ciudad', $filas['nombreciudad']);
                    $ciudad = $direccion->appendChild($ciudad);
                //NODO PADRE
                $color = $xml->createElement('Color');
                $color = $chat->appendChild($color);
                    //NODO HIJO
                    $idcolor = $xml->createElement('color', $filas['color']);
                    $idcolor = $color->appendChild($idcolor);
                //NODO PADRE
                $mensaje = $xml->createElement('Mensaje');
                $mensaje = $chat->appendChild($mensaje);
                    //NODO HIJO
                    $mensaje2 = $xml->createElement('mensaje', $filas['mensaje']);
                    $mensaje2 = $mensaje->appendChild($mensaje2);
                //NODO PADRE
                $fecha = $xml->createElement('fecha', (date("d-m-Y H:i", strtotime($filas['fecha']))));
                $fecha = $chat->appendChild($fecha);
            }
            //Modo de guardado y lectura
            $xml->save($fn);
            readfile($fn);
        }     
        else 
        {
            echo "No hay datos";
        }
        break;

    default:
        break;
}



/*
//RESPALDO Y OTRA FORMA POR CONEXION pg....
    public function descargarXML()
    {
        $pgsql = getConnect();
        $query = "SELECT * FROM chat.vw_xml";
        $resultado = pg_query_params($pgsql, $query, array());
        $error = pg_last_error($pgsql);
        if ($error != '') {
            die($error);
        }
        header('Content-type: text/xml');
        header('Content-Disposition: attachment; filename="chat.xml"');
        //$json = array();
        $nr = pg_num_rows($resultado);
        if ($nr > 0) {
            $xml = new DomDocument('1.0', 'UTF-8');
            $xml->formatOutput = true;
            $chat2 = $xml->createElement('Chats');
            $chat2 = $xml->appendChild($chat2);

            while ($filas = pg_fetch_array($resultado)) {
                $chat = $xml->createElement('chat');
                $chat = $chat2->appendChild($chat);

                $persona = $xml->createElement('persona');
                $persona = $chat->appendChild($persona);

                $idchat = $xml->createElement('idchat', $filas['idchat']);
                $idchat = $persona->appendChild($idchat);

                $nombre = $xml->createElement('nombre', $filas['nombreapellido']);
                $nombre = $persona->appendChild($nombre);

                $alias = $xml->createElement('alias', $filas['alias']);
                $alias = $persona->appendChild($alias);

                $rut = $xml->createElement('rut', $filas['rut']);
                $rut = $persona->appendChild($rut);

                $email = $xml->createElement('email', $filas['email']);
                $email = $persona->appendChild($email);

                $direccion = $xml->createElement('Direccion');
                $direccion = $persona->appendChild($direccion);

                $idpais = $xml->createElement('idpais', $filas['idpais']);
                $idpais = $direccion->appendChild($idpais);

                $pais = $xml->createElement('pais', $filas['nombrepais']);
                $pais = $direccion->appendChild($pais);

                $idciudad = $xml->createElement('idciudad', $filas['idciudad']);
                $idciudad = $direccion->appendChild($idciudad);

                $ciudad = $xml->createElement('ciudad', $filas['nombreciudad']);
                $ciudad = $direccion->appendChild($ciudad);

                $color = $xml->createElement('Color');
                $color = $chat->appendChild($color);

                $color2 = $xml->createElement('color', $filas['color']);
                $color2 = $color->appendChild($color2);

                $mensaje = $xml->createElement('Mensaje');
                $mensaje = $chat->appendChild($mensaje);

                $mensaje2 = $xml->createElement('mensaje', $filas['mensaje']);
                $mensaje2 = $mensaje->appendChild($mensaje2);

                $fecha = $xml->createElement('fecha', (date("d-m-Y H:i", strtotime($filas['fecha']))));
                $fecha = $chat->appendChild($fecha);
            }
            $xml->save('chat.xml');
            readfile('chat.xml');

        } else {
            //echo "No hay datos";
        }

    }
*/